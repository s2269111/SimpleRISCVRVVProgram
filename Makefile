CC=riscv64-unknown-linux-gnu-gcc
CFLAGS=-O2 -march=rv64gcv -mabi=lp64d
TARGET=rvv-test
SRCFILE=$(TARGET).c

$(TARGET):$(SRCFILE)
    $(CC) $(CFLAGS) -o $(TARGET) $(SRCFILE)

clean:
    rm $(TARGET)
